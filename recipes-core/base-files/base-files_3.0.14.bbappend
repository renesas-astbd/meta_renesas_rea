ROOT_HOME = "/root"

do_install_append () {
	echo "  export XDG_RUNTIME_DIR=/run/user/##USER##" >> ${D}/${sysconfdir}/skel/.profile
	echo "  export DISPLAY=0:0" >> ${D}/${sysconfdir}/skel/.profile

	echo "if [ \"0\" == \"\`id -u\`\" ];" >> ${D}/${sysconfdir}/skel/.bashrc
	echo "then" >> ${D}/${sysconfdir}/skel/.bashrc
	echo "  export PS1=\"\\[\\033[01;31m\\]\\u@\\h\\[\\033[00m\\]:\\[\\033[01;34m\\]\\w\\a\\\[\\033[00m\\]# "\" >> ${D}/${sysconfdir}/skel/.bashrc
	echo "else" >> ${D}/${sysconfdir}/skel/.bashrc
	echo "  export PS1=\"\\[\\033[01;32m\\]\\u@\\h\\[\\033[00m\\]:\\[\\033[01;34m\\]\\w\\a\\\[\\033[00m\\]\$ "\" >> ${D}/${sysconfdir}/skel/.bashrc
       echo "fi" >> ${D}/${sysconfdir}/skel/.bashrc
	sed -i -e 's/##USER##/\$\{USER\}/' ${D}/${sysconfdir}/skel/.profile

	install -m 0755 ${D}${sysconfdir}/skel/.profile ${D}/root/.profile
	install -m 0755 ${D}${sysconfdir}/skel/.bashrc  ${D}/root/.bashrc
}
