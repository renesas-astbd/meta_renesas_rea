SUMMARY = "Rogue: Exploring the Dungeons of Doom"
DESCRIPTION = "The venerable Rogue by Ken Arnold, Michael Toy, and Glenn Wichman"
SECTION = "games"

inherit autotools gettext

HOMEPAGE = "http://rogue.rogueforge.net/"
LICENSE= ""
#LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE.TXT;md5=dc87706a4dd6f6f35ae78aa8c12a96cc"
LIC_FILES_CHKSUM = "file://LICENSE.TXT;md5=dc87706a4dd6f6f35ae78aa8c12a96cc"

PR = "r0"
SRC_URI = "http://rogue.rogueforge.net/files/rogue5.4/rogue5.4.4-src.tar.gz"

SRC_URI[md5sum] = "033288f46444b06814c81ea69d96e075"
SRC_URI[sha256sum] = "7d37a61fc098bda0e6fac30799da347294067e8e079e4b40d6c781468e08e8a1"

PN = 'rogue'
PV = '5.4.4'

# The rogue tarball doesn't have a hypen in the tar'd directory
S = '${WORKDIR}/${PN}${PV}'

#ENABLE_WIDEC ?= "true"

DEPENDS_${PN} = "ncurses"
RDEPENDS_${PN} = "ncurses"


addtask configure
do_configure() {
    ./configure --host=arm
}

addtask install
do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/rogue${PV}/rogue ${D}${bindir}/
}
