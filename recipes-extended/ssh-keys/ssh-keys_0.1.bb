DESCRIPTION = "ssh public key packages"
HOMEPAGE = "http://www.yoctoproject.org"
BUGTRACKER = "http://bugzilla.pokylinux.org"

PR = "r0"

LICENSE = "CLOSED"

PACKAGES = "${PN}-client ${PN}-server"

S = "${WORKDIR}"

USER_ROOT="root"
USER_RCAR="rcar"

do_install() {
        install -d ${D}/${USER_ROOT}/.ssh/
	install -m 0755 ~/.ssh/id_rsa.pub ${D}/${USER_ROOT}/.ssh/
	install -m 0755 ~/.ssh/id_rsa.pub ${D}/${USER_ROOT}/.ssh/authorized_keys


        install -d ${D}/home/${USER_RCAR}/.ssh/
	install -m 0755 ~/.ssh/id_rsa.pub ${D}/home/${USER_RCAR}/.ssh/
	install -m 0755 ~/.ssh/id_rsa.pub ${D}/home/${USER_RCAR}/.ssh/authorized_keys
        
}

FILES_${PN}-client = "/${USER_ROOT}/.ssh/id_rsa.pub"
FILES_${PN}-server = "/${USER_ROOT}/.ssh/authorized_keys"

FILES_${PN}-client += "/home/${USER_RCAR}/.ssh/id_rsa.pub"
FILES_${PN}-server += "/home/${USER_RCAR}/.ssh/authorized_keys"

