DESCRIPTION = "OpenGL 1.x driver for OpenGL ES devices"

HOMEPAGE = "https://github.com/lunixbochs/glshim"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=6e6c6ebde7cf6a226f61f9790990d1eb"

SRC_URI = "git://github.com/lunixbochs/glshim.git;protocol=https;nobranch=1"

SRCREV = "476e1b6c277d435c18feaf5f05839fe847655925"

S = "${WORKDIR}/git"

DEPENDS = "gtk+ virtual/egl"

inherit cmake
