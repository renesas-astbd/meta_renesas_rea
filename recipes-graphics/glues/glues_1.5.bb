DESCRIPTION = "GLU ES (version 1.5)"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d44ed0146997856304dfbb512a59a8de"

PN = "glues"
PR = "r0"

GLUES_REPO = "git://github.com/lunixbochs/glues.git"

SRC_URI = "${GLUES_REPO};protocol=git;branch=glu"
SRCREV = "44cb7c6eae4488f921041572908f3af508880547"

S = "${WORKDIR}/git"
DEPENDS = "glshim mesa"

inherit cmake pkgconfig

do_install(){
    install -d ${D}${libdir}
    install -m 775 libGLU.so.1 ${D}${libdir}
    cd ${D}${libdir}
    cp libGLU.so.1 libGLU.so.1.0.0
    ln -sf libGLU.so.1 libGLU.so
}

PACKAGES = " \
    ${PN} \
    ${PN}-dev \
    ${PN}-dbg \
"

FILES_${PN} = "${libdir}/libGLU.so.*"
FILES_${PN}-dev = "${libdir}/libGLU.so"
FILES_${PN}-dbg = " \
    /usr/src/debug/* \
    ${libdir}/.debug/* \
"