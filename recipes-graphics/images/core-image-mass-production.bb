include meta-rcar-gen2/recipes-graphics/images/core-image-renesas-base.inc

SUMMARY = "A small image just capable of allowing a device to boot"

IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL}"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"

DISTRO_FEATURES_remove = "x11 wayland"
