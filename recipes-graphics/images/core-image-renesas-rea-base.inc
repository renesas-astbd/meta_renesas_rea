###
# R-Car configuration of Renesas Electronics of America (REA)
#

# Enable Multimedia standard pkg
MACHINE_FEATURES += " multimedia"

###
# BitBake Debugging Target
#

# Examples
IMAGE_INSTALL += " hello"
#IMAGE_INSTALL += " rogue" //not working in v2.7.0

# Useful utilities
IMAGE_INSTALL += " nano"
IMAGE_INSTALL += " screen"
IMAGE_INSTALL += " rsync"

# User management & security
IMAGE_INSTALL += " sudo"

# Additional GUI/dev kits
IMAGE_INSTALL += " gdb"
IMAGE_INSTALL += " boost"
#IMAGE_INSTALL += "wxwidgets"  //Requires GLU/GLUES

# Options to aid debugging
EXTRA_IMAGE_FEATURES ?= "debug-tweaks"
EXTRA_IMAGE_FEATURES ?= "tools-profile"
EXTRA_IMAGE_FEATURES ?= "tools-testapps"
EXTRA_IMAGE_FEATURES ?= "tools-debug"
EXTRA_IMAGE_FEATURES ?= ""
EXTRA_IMAGE_FEATURES ?= ""

# Performance Profiling
IMAGE_INSTALL += " perf"

# SSH authorized key
IMAGE_INSTALL += " ssh-keys-client ssh-keys-server"

# Support GLUES
#IMAGE_INSTALL_append = " glues" //not working v2.7.0

# On-target debugging
#EXTRA_IMAGE_FEATURES += " dbg-pkgs"

# Self-hosting development
EXTRA_IMAGE_FEATURES += " tools-sdk dev-pkgs"
#IMAGE_INSTALL += " autoconf automake binutils binutils-symlinks cpp cpp-symlinks gcc gcc-symlinks g++ g++-symlinks make libstdc++ libstdc++-dev file gettext file coreutils"
#IMAGE_INSTALL += " pkgconfig glib-2.0"
#IMAGE_INSTALL += " pkgconfig mesa-dev"
IMAGE_INSTALL += " git"
IMAGE_INSTALL += " gcc g++ coreutils"

# Enable XWayland
#DISTRO_FEATURES_append = " x11 wayland xwayland"

# Include X11 with Weston build so we can run XWayland
#IMAGE_INSTALL += " x11-sato"
#IMAGE_INSTALL += " packagegroup-core-x11-sato-games"

# Needed for startx to function
#IMAGE_INSTALL += " xterm"
#IMAGE_INSTALL += " twm"
#IMAGE_INSTALL += " xclock"

# C# support
#IMAGE_INSTALL += " mono"

# Add rcar user
inherit extrausers
EXTRA_USERS_PARAMS = " \
    useradd -p '' rcar; \
    usermod -a -G  input,audio,video,tty,sudo rcar; \
"

###
# BitBake Production Target
#

