require recipes-graphics/images/core-image-weston.bb
include meta-rcar-gen2/recipes-graphics/images/core-image-weston.inc
include core-image-weston.inc

DESCRIPTION = " \
    Image with weston support that includes everything within \
    core-image-weston plus debug-toolchain \
"

IMAGE_FEATURES += "dev-pkgs dbg-pkgs tools-sdk tools-debug"

