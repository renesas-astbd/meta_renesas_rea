include core-image-weston.inc

user_postprocess_function() {
   echo " export PATH=/sbin:\$PATH" >>${IMAGE_ROOTFS}/etc/skel/.profile
   cp ${IMAGE_ROOTFS}/etc/skel/.profile ${IMAGE_ROOTFS}/home/rcar/.profile
}

ROOTFS_POSTPROCESS_COMMAND_append = " \
  user_postprocess_function; \
"

