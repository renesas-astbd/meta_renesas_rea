require recipes-graphics/images/core-image-x11.bb
include meta-rcar-gen2/recipes-graphics/images/core-image-x11.inc
include core-image-x11.inc

DESCRIPTION = " \
    Image with x11 support that includes everything within \
    core-image-x11 plus debug-toolchain \
"

IMAGE_FEATURES += "dev-pkgs dbg-pkgs tools-sdk tools-debug "

