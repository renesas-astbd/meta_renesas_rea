SUMMARY = "wxWidgets"
DESCRIPTION = "Cross-platform GUI library with native look-and-feel"
SECTION = "GUI"

inherit autotools pkgconfig
#gettext
#inherit autotools-brokensep
#inherit autotools-brokensep pkgconfig binconfig

HOMEPAGE = "http://www.wxwidgets.org"
LICENSE= "wxWindows"
LIC_FILES_CHKSUM = "file://docs/licence.txt;md5=18346072db6eb834b6edbd2cdc4f109b"

PR = "r0"
SRC_URI = "https://github.com/wxWidgets/wxWidgets/releases/download/v${PV}/wxWidgets-${PV}.tar.bz2"

SRC_URI[md5sum] = "ba4cd1f3853d0cd49134c5ae028ad080"
SRC_URI[sha256sum] = "346879dc554f3ab8d6da2704f651ecb504a22e9d31c17ef5449b129ed711585d"

FILES_${PN}-dbg = "/usr/src /lib/.debug /bin/.debug"
FILES_${PN}-locale = "/share/locale"
FILES_${PN}-dev = "/include /lib/*.a"
#FILES_${PN}-doc = ""
FILES_${PN} = "/share /bin /lib /usr"

PACKAGES = "${PN}-dbg ${PN}-locale ${PN}-dev ${PN}"

DEPENDS += "gtk+ jpeg tiff libpng zlib expat libxinerama glues "
DEPENDS += "gstreamer1.0-omx "
#DEPENDS += "gstreamer1.0 "
RDEPENDS_${PN} += "gtk+"

# These are used by the autotools do_configure which I could not get to work
#EXTRA_OECONF += "--enable-stl --enable-unicode --prefix=${D}"
#EXTRA_OECONF_remove += "--enable-nls"

#before do_configure
#before do_patch
addtask movesrc after do_unpack before do_populate_lic
do_movesrc() {
    cd ${S}
    cd ..
    if [ -d "wxWidgets-${PV}" ]; then
       rm -rf wxwidgets-${PV}
       mv wxWidgets-${PV} wxwidgets-${PV}
       cd ${S}
    fi
}

addtask configure after do_movesrc
do_configure() {
    #mkdir -p ${S}/gtk-build
    #cd ${S}/gtk-build
    cd ${S}
    #--enable-monolithic
    if [ ! -e "Makefile" ]; then
        echo CONFIGURING        
        ./configure --host=arm-poky-linux-gnueabi --enable-stl --enable-unicode --disable-webview --prefix=${D}
    fi
}

#addtask compile after do_configure
#do_compile() {
#    cd ${S}/gtk-build
#    make -j12
#}

addtask install after do_compile
do_install() {
    echo PACKAGES:${PACKAGES}
    #cd gtk-build
    make install
    rm ${D}/lib/*arm-poky-linux-gnueabi*
}
