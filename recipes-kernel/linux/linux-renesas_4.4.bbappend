DESCRIPTION = "REA Linux kernel tweaks"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}/${MACHINE}:"
COMPATIBLE_MACHINE = "salvator-x"

SRC_URI_append += " \
    file://tick.cfg \
    file://usb_audio.cfg \

IMAGE_INSTALL_append += " kernel-modules"
